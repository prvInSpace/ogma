# Ogma Grammar Matching and Analysis

[![pipeline status](https://gitlab.com/prvInSpace/ogma/badges/master/pipeline.svg)](https://gitlab.com/prvInSpace/ogma/-/commits/master)
[![coverage report](https://gitlab.com/prvInSpace/ogma/badges/master/coverage.svg)](https://gitlab.com/prvInSpace/ogma/-/commits/master)
[![License: MIT](https://img.shields.io/badge/License-MIT-blue.svg)](https://opensource.org/licenses/MIT)

Ogma is a tool for text searching that is aimed at making it simpler to search through text while making it simpler to take into consideration grammatical phenomenons such as conjugation, mutation, or even word classes. It is aiming to be a simple domain-specific language that allows the user to define search patterns with some simple regex constants and functions while also allowing for linking with other programming languages to provide runtime functions.

## Formal Language Definition

The formal language definition can be found [here.](https://gitlab.com/prvInSpace/ogma/-/blob/master/documentation/language_definition.md)

## Additional Documentation

Additional documentation can be found on the Gitlab Pages for the project and is automatically deployed when a new version is merged into master.

* [JavaDoc](https://prvinspace.gitlab.io/ogma/javadoc/)
* [Coverage (JaCoCo)](https://prvinspace.gitlab.io/ogma/jacoco/)

## Importing Ogma into Your Project

**NB: Due to the project being private this does not currently work without additional authentication**

The latest version of the library is hosted through the package repository here on Gitlab.
That means that you are able to import the library using a several different build tools. 

### Import Ogma into Gradle

In the list of repositories in your build.gradle file add the following repository:
```
repositories {
    maven {
        url "https://gitlab.com/api/v4/projects/23940158/packages/maven"
        name "GitLab"
    }
}
```

And then in the list of dependencies add a reference to the library like this:
```
dependencies {
    implementation 'cymru.prv:ogma:+'
}
```

## Building Ogma from the Commandline

Ogma uses Java 11 and uses Gradle to build the project.

### Setting up Java 11
Make sure you have Java 11 installed and the java command line set to the correct verion
On Arch-based distributions:
```
sudo archlinux-java set java-11-openjdk
```

On Debian-based distributions:
```
update-java-alternatives --list sudo update-java-alternatives --set /path/to/java/version
```

### Building Jar-file

Then go in the project directory and run:
```
./gradlew build
```

The Jar-file will then be available in the following path (exact path of the jar file may vary):
```
build/libs/Ogma\ Grammar\ Matching\ and\ Analysis-1.0-SNAPSHOT.jar
```

## Maintainer

* Preben Vangberg &lt;prv@aber.ac.uk&gt;
