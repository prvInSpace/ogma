# Ogma Language Defintion Document

Ogma is a language that allows for more complicated searching with named components, functions, and even external evaluation of tokens in a text.
This document try to explain the different components that builds up the Ogma language.

## Constants

Constants are named Regex literals that can be reused other places in the application.
Constants are defined as a list of components, whether that be Regex, other constants, or function calls.

Since they return a single Regex literal they can not contain any matchers.

A constant declaration looks like this:
```
const <name> = <list of components>
```

An example using string literals, other named components, and function calls:
```
const myConstant = "^hello" myOtherConstant myFunction("Hello, World")
```

## Functions

There are two Regex-resovlable functions in Ogma: Ogma defined Regex functions and external regex functions.
These functions work very similar to constants in that both return Regex literals.

#### Ogma Regex Function

Ogma Regex functions work very similar to constants in that it is built up by a list of components and return a single regex literal.
The only difference is that it takes parameters which can also be used to building this literal.

As with constants components can be both other function calls, regex literals, or named constants.

Parameters are named with a $ then the number. It is 1 indexed, which means that the first parameter is named $1.
A function might take 0 arguments.

Example of a function declaration:
```
function myFunction(1) = "^" $1 "$"
```

Example of a function call:
```
myFunction("Hello, World")
```

#### External Regex Function

An external function works exactly the same as a regex function defined in Ogma.
However, it is implemented externaly and linked with the program.
These functions needs to be declared before they are used.

## Matchers

A matcher is a boolean expression that is used to determine whether a token is considered a match or not.
These matchers are used to indicate whether an token is optional or required, and contains a logical expression of whether the token from the text matches the token in the search pattern.
These logical expressions might contain logical operators, regex matchers, runtime functions, or other matchers.

### Named matchers

Matchers can be predefined before being used in a search pattern.
They can be defined like this:

```
matcher myMatcher = "hello" & lemma("test") | !mySecondMatcher
```

The matcher can then be used in search patterms like this:

```
pattern myPattern = {myMatcher|"otherWord"}(noun)
```

### Logical Operators

There are some logical operators available in Ogma.
These are:

* And: &
* Or: |
* Not: !

Other than these parentheses can be used to make more complicated logical expressions if needed.


### External Matcher Function

An external matcher function is an external token matcher function that returns a boolean value on whether a token matches a given criterium.
These functions are named the same way as constants and functions and the call syntax is the same.
However, these functions are implemented in the programming language of choice.

These functions might only be used in search patterns and might not be used as part of defining other named components.


## Search Patterns

Search patterns define a pattern that the user might want to search for in a given text.
There is no boundary on the amount of patterns that can be defined.

A pattern is built up by 'token matchers'.
There is more information about matchers in the section called 'Matchers'.

One of the words might be tagged as a "target token" which acts the same way as a required token, however when handling the match afterwards you will get information about the target word as well as the entire match.

Example of an unamed search pattern:
```
{def}[adj](noun)
```

### Named Patterns

Patterns can also be given an identifier.

Example declaration of a named pattern:
```
pattern myPattern = {noun}(adj)
```

Note: If patterns are not given an identifier the identifier for the pattern will be the pattern itself.


### Optional token

Optional tokens are defined using square brackets.

### Required token

Required tokens are defined using curly brackets.

### Target token

The target token is defined using normal parentheses.


### Groups

Search patterns can also be placed in a group. This gives more information about which patterns gave rise to a match.
These groups can be nested.

Example usage of groups:
```
group MyGroup
	{noun}(adj)
	{def}(noun)
	group MyNestedGroup
		{def}(adj){noun}
	end
end
```

## Comments

* A single line comment is a line that starts with a %.
* A multi-line comment is a line that starts with a %% and runs until it finds a line that starts with a %%.

