package cymru.prv.ogma;

/**
 * An enum representing the different types
 * of matches that can occur when searching.
 *
 * Found -> Token was found
 * Mismatch -> Expected token was not found
 * Optional_Mismatch -> Optional token was not found
 *
 * @author Preben Vangberg
 * @since 1.0.0
 */
enum MatchType {
    FOUND, MISMATCH, OPTIONAL_MISMATCH
}
