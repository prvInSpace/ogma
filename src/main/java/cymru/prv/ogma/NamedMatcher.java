package cymru.prv.ogma;

import java.util.function.Function;


/**
 * Represent a named matcher expression
 *
 * @author Preben Vangberg
 * @since 1.0.0
 */
class NamedMatcher implements NamedComponent {

    private final String name;
    private final Matcher matcher;


    /**
     * Parses the matcher declaration
     *
     * @param parser Parser where the declaration is stored
     */
    NamedMatcher(OgmaParser parser){
        parser.popAndThrow("matcher");
        name = parser.pop();
        matcher = new ParenthesisMatcher(parser, "=", null);
    }


    /**
     * Returns the value of the matcher expression
     *
     * @param parser The parser where the call is stored
     * @return A reference to the internal matcher
     */
    @Override
    public Function<String, Boolean> getMatcher(OgmaParser parser) {
        parser.popAndThrow(name);
        return matcher::isMatch;
    }


    /**
     * The name of the component
     * @return The name of the component
     */
    @Override
    public String getName() {
        return name;
    }
}
