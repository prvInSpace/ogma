package cymru.prv.ogma;


/**
 * Represent a single OR operator
 *
 * @author Preben Vangberg
 * @since 1.0.0
 */
class OrMatcher extends Matcher {

    private final Matcher leftSide;
    private final Matcher rightSide;

    /**
     * Parses the right side of the operator
     * and stores the left side internally.
     *
     * @param parser The parser to use
     * @param leftSide The left side of the operator
     */
    OrMatcher(OgmaParser parser, Matcher leftSide){
        this.leftSide = leftSide;
        parser.popAndThrow("|");
        rightSide = parseMatcher(parser);
    }


    /**
     * Returns true if either of the matchers return
     * true. Return false otherwise
     *
     * @param word The token to check
     * @return Returns true if either of the matcher returns true.
     */
    @Override
    public boolean isMatch(String word) {
        return leftSide.isMatch(word) || rightSide.isMatch(word);
    }
}
