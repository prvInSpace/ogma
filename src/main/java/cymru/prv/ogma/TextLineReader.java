package cymru.prv.ogma;

import java.io.BufferedReader;
import java.io.IOException;

/**
 * A very simple implementation of an OgmaReader
 * that splits the text on every newline.
 * each word is split by whitespace
 *
 * @author Preben Vangberg
 */
public class TextLineReader implements TextReader {

    private final BufferedReader reader;


    /**
     * Creates a new instance of the reader with
     * the given reader as its source.
     *
     * @param reader The reader to read text from
     */
    public TextLineReader(BufferedReader reader) {
        this.reader = reader;
    }


    /**
     * Fetches the next line and splits it into
     * tokens based on whitespace
     *
     * @return An array of tokens
     * @throws IOException _
     */
    @Override
    public String[] getNext() throws IOException {
        String line = reader.readLine();
        if(line == null)
            return null;
        return line.split("\\s");
    }

}
