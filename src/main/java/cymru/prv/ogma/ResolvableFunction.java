package cymru.prv.ogma;

import java.util.function.Function;
import java.util.regex.Pattern;


/**
 * Represents a single resolvable function
 * The matcher works in the same way for the
 * both the internal and external Regex functions
 *
 * @author Preben Vangberg
 * @since 1.0.0
 */
public abstract class ResolvableFunction extends AbstractFunction implements Resolvable {


    /**
     * Compiles the resolved version of the function into
     * a pattern and returns a lambda function that matches
     * any string to this matcher.
     *
     * @param parser The parser where the call is stored
     * @return A function that matches any token to the resolved version of the function
     */
    @Override
    public Function<String, Boolean> getMatcher(OgmaParser parser) {
        Pattern pattern = Pattern.compile(getStringForm(parser));
        return s -> pattern.matcher(s).find();
    }

}
