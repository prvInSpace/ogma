package cymru.prv.ogma;

import java.util.function.BiFunction;
import java.util.function.Function;


/**
 * Represents an external matcher function.
 * Essentially only stores a reference to
 * the external function so that it can be
 * used as a matcher.
 *
 * @author Preben Vangberg
 * @since 1.0.0
 */
class ExternalMatcherFunction extends AbstractFunction {


    private final String name;
    private final BiFunction<String, String[], Boolean> function;


    /**
     * Creates an external matcher function based on
     * the name and the function pointer.
     *
     * @param name Name of the component
     * @param function A pointer to the function
     */
    ExternalMatcherFunction(String name, BiFunction<String, String[], Boolean> function){
        this.name = name;
        this.function = function;
    }


    /**
     * Parses the parameters and creates a lambda expression
     * to the matcher function with the given parameters.
     *
     * @param parser The parser where the call is stored
     * @return A lambda call to the external function
     */
    @Override
    public Function<String, Boolean> getMatcher(OgmaParser parser) {
        parser.popAndThrow(name);
        String[] arguments = parseParameters(parser);
        return (s) -> function.apply(s, arguments);
    }


    /**
     * The name of the component
     * @return The name of the component
     */
    @Override
    public String getName() {
        return name;
    }
}
