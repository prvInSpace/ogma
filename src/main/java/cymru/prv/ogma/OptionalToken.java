package cymru.prv.ogma;

/**
 * Represents a an optional token in a search pattern
 *
 * Optional tokens are represented by square brackets
 *
 * @author Preben Vangberg
 * @since 1.0.0
 */
class OptionalToken implements TokenMatcher {

    private final Matcher matcher;

    /**
     * Parses the token matcher.
     *
     * @param parser The parses to use
     */
    OptionalToken(OgmaParser parser){
        matcher = new ParenthesisMatcher(parser, "[", "]");
    }


    /**
     * If the token matches it return FOUND
     * Otherwise it returns OPTIONAL_MISMATCH
     *
     * @param word The word to check
     * @return A match type indicating whether it was found or not
     */
    @Override
    public MatchType getMatchType(String word) {
        if(matcher.isMatch(word))
            return MatchType.FOUND;
        return MatchType.OPTIONAL_MISMATCH;
    }
}
