package cymru.prv.ogma;

import java.io.IOException;

/**
 * Defines how a sequence of text should be
 * split up and read. It contains one
 * function that is meant to return the
 * next sequence of text that is to be
 * searched through.
 *
 * @author Preben Vangberg
 * @since 1.0.0
 */
public interface TextReader {

    /**
     * Fetches the sequence of words to search in.
     * @return the next sequence of words
     * @throws IOException if unable to read the next line
     */
    String[] getNext() throws IOException;
}
