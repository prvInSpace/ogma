package cymru.prv.ogma;

import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;


/**
 * Represents a single search pattern in the Ogma language
 *
 * Can be both anonymous or named.
 *
 * @author Preben Vangberg
 * @since 1.0.0
 */
class SearchPattern {

    private final List<TokenMatcher> matchers = new LinkedList<>();
    private final List<String> groups;

    private final String name;

    private int shortestLength = 0;

    /**
     * Parses a single anonymous or named search pattern from
     * the content of the parser.
     *
     * @param parser The parser that contains the declaration
     */
    SearchPattern(OgmaParser parser){

        if(parser.peek().equals("pattern")){
            parser.popAndThrow("pattern");
            name = parser.pop();
            parser.popAndThrow("=");
        }
        else
            name = parser.getCurrentLine();

        groups = parser.getCurrentGroups();
        int foundTargets = 0;

        while(parser.hasNext()){
            switch (parser.peek()){
                case "(":
                    matchers.add(new TargetToken(parser));
                    foundTargets++;
                    shortestLength++;
                    break;
                case "{":
                    matchers.add(new RequiredToken(parser));
                    shortestLength++;
                    break;
                case "[":
                    matchers.add(new OptionalToken(parser));
                    break;
                default:
                    throw new SyntaxErrorException(parser, "Unexpected token " + parser.peek());
            }
        }

        if(foundTargets == 0)
            throw new SyntaxErrorException(parser, "Pattern contains no target");
        else if(foundTargets > 1)
            throw new SyntaxErrorException(parser, "Pattern contains more than one target");
    }


    /**
     * Creates a list of all the matches that the search pattern
     * finds in the list of tokens.
     *
     * @param tokens The tokens to search through
     * @return A list of matches
     */
    List<Match> getMatches(List<String> tokens){
        List<Match> matches = new LinkedList<>();

        String target = null;
        int targetIndex = -1;

        int i = 0;
        int start = 0;
        int matcherIndex = 0;

        while(i < tokens.size()){
            MatchType matchType = matchers.get(matcherIndex).getMatchType(tokens.get(i));

            // If we found the token step forwards
            if(matchType == MatchType.FOUND){
                if(matchers.get(matcherIndex) instanceof TargetToken) {
                    target = tokens.get(i);
                    targetIndex = i;
                }
                i++;
                matcherIndex++;

                // We have reached the end of the pattern which means we have a match
                if(matcherIndex == matchers.size()){
                    matches.add(new Match(this, tokens.subList(start, i), target, targetIndex, tokens));
                    start++;
                    i = start;
                    matcherIndex = 0;
                }
            }

            // If we have an have a mismatch and the token is optional
            // go to the next matcher.
            else if(matchType == MatchType.OPTIONAL_MISMATCH){
                matcherIndex++;
            }

            // If not restart at the start of the pattern
            else {
                start++;
                i = start;
                matcherIndex = 0;
                if(start > tokens.size() - shortestLength)
                    break;
            }
        }

        return matches;
    }


    /**
     * The name of the search pattern. Either the
     * supplied name or a generated one.
     *
     * @return The name of the search pattern.
     */
    public String getName() {
        return name;
    }


    /**
     * Fetches the groups of the pattern.
     * @return The groups of the pattern.
     */
    public Collection<String> getGroups() {
        return Collections.unmodifiableCollection(groups);
    }
}
