package cymru.prv.ogma;

import java.io.IOException;
import java.io.Reader;
import java.util.*;
import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.function.Function;

/**
 * Represents an instance of an Ogma program.
 * Information about named components and functions are stored inside
 * of it.
 *
 * @author Preben Vangberg
 * @since 2021.02.13
 */
public class Ogma {

    private final Map<String, NamedComponent> namedComponents = new HashMap<>();
    private final List<SearchPattern> patterns = new LinkedList<>();


    /**
     * Add named regex constant to the Ogma instance
     * @param name the name of the regex constant
     * @param literal the regex literal
     */
    public void addStringLiteral(String name, String literal){
        namedComponents.put(name, new Constant(name, literal));
    }


    /**
     * Add a regex function to the Ogma instance.
     * A Regex function should be a function that takes an array of parameters
     * and return a regex string.
     * @param name the name of the function
     * @param function a pointer to the function
     */
    public void addRegexFunction(String name, Function<String[], String> function){
        namedComponents.put(name, new ExternalRegexFunction(name, function));
    }


    /**
     * Add a runtime function to the Ogma instance.
     * A runtime function is a function that can be used in search patterns to allow for
     * more advanced matching.
     * The function should take the word and a list of parameters and should return a boolean
     * of whether the word is a match or not.
     * @param name The name of the function
     * @param function a pointer to the function
     */
    public void addMatcherFunction(String name, BiFunction<String, String[], Boolean> function){
        namedComponents.put(name, new ExternalMatcherFunction(name, function));
    }


    /**
     * Takes the reader and parses the Ogma program in it.
     * The content parsed from the reader is then inserted
     * into the Ogma instance
     *
     * @throws SyntaxErrorException if the parses stops prematurely due to a syntax error
     * @param reader A BufferedReader with the code of the Ogma program
     */
    public void parseOgmaFile(Reader reader){
        OgmaParser.parse(this, reader);
    }


    /**
     * Takes the reader and parses the Ogma program in it.
     * The content parsed from the reader is then inserted
     * into the Ogma instance
     *
     * @throws SyntaxErrorException if the parses stops prematurely due to a syntax error
     * @param reader A BufferedReader with the code of the Ogma program
     */
    public void parseOgmaFile(Scanner reader){
        OgmaParser.parse(this, reader);
    }


    /**
     * Reads through all the sequences of text in the OgmaReader and
     * sends any matches to the callback function to be handled
     *
     * @throws IOException if the reader throws an exception
     * @param callback the callback function that accepts matches when
     *                 they are found
     */
    public void findMatches(TextReader reader, Consumer<Match> callback) throws IOException{
        String[] line;
        while ((line = reader.getNext()) != null) {
            for(SearchPattern pattern : patterns){
                List<Match> matches = pattern.getMatches(Arrays.asList(line));
                for (Match m : matches)
                    callback.accept(m);
            }
        }
    }

    /*
     * Internal functions
     */

    /**
     * Fetches the named component with the
     * given name.
     *
     * @param name The name of the component
     * @return The component with the given name
     */
    NamedComponent getNamedComponent(String name){
        return namedComponents.get(name);
    }


    /**
     * Adds a named component to the Ogma instance
     *
     * @param component The component to add
     * @param parser In case an exception is caught
     */
    void addNamedComponent(NamedComponent component, OgmaParser parser){
        if(namedComponents.containsKey(component.getName()))
            throw new SyntaxErrorException(parser, "Named component " + component.getName() + " already exists");
        namedComponents.put(component.getName(), component);
    }


    /**
     * Adds a named component to the map of
     * named components
     *
     * Used as a helper function for testing
     *
     * @param component The named component to add
     */
    void addNamedComponent(NamedComponent component){
        addNamedComponent(component, new OgmaParser(""));
    }


    /**
     * Adds the given search pattern to the list
     * of search patterns
     *
     * @param searchPattern The search pattern to add
     */
    void addSearchPattern(SearchPattern searchPattern){
        patterns.add(searchPattern);
    }

}
