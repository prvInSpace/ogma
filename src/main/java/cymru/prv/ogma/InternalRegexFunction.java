package cymru.prv.ogma;

import java.util.LinkedList;
import java.util.List;
import java.util.function.Function;


/**
 * Represent a single internal regex function.
 *
 * Takes a set amount of parameters and returns
 * a single Regex expression. Regex functions
 * are resolvable.
 *
 * @author Preben Vangberg
 * @since 1.0.0
 */
class InternalRegexFunction extends ResolvableFunction {

    private final String name;
    private final int numberOfArguments;
    private final List<Function<String[], String>> components = new LinkedList<>();
    private final OgmaParser parser;


    /**
     * Parses the function declaration
     *
     * @param parser The parser to fetch the tokens from
     */
    InternalRegexFunction(OgmaParser parser){
        this.parser = parser;
        parser.popAndThrow("function");
        name = parser.pop();
        parser.popAndThrow("(");

        if(parser.peek().equals(")"))
            numberOfArguments = 0;
        else {
            numberOfArguments = Integer.parseInt(parser.pop());
        }

        parser.popAndThrow(")");
        parser.popAndThrow("=");

        while(parser.hasNext()){
            if(parser.peek().equals("\"")) {
                parser.popAndThrow("\"");
                String value = parser.pop();
                components.add(s -> value);
                parser.popAndThrow("\"");
            }
            else if(parser.peek().equals("$")) {
                parser.popAndThrow("$");
                int paramNumber = Integer.parseInt(parser.pop());
                components.add((s) -> s[paramNumber-1]);
            }
            else {
                components.add(parseNestedFunctionCall(parser));
            }
        }
    }


    /**
     * Parses nested function calls.
     *
     * These are a bit different from normal function calls as they
     * have to be parsed before calling due to the parameters being
     * defined on the top level.
     *
     * @param parser The parser to fetch the tokens from
     * @return An lambda expression to the function call
     */
    Function<String[], String> parseNestedFunctionCall(OgmaParser parser){

        String function = parser.pop();

        // If it is a constant
        if(!parser.hasNext() || !parser.peek().equals("(")) {
            String value = parser.getResolvableComponent(function).getStringForm(new String[0]);
            return (s) -> value;
        }

        parser.popAndThrow("(");

        List<Function<String[], String>> nestedParams = new LinkedList<>();
        boolean first = true;
        while(parser.hasNext() && !parser.peek().equals(")")){
            if(!first)
                parser.popAndThrow(",");
            first = false;
            if(parser.peek().equals("\"")) {
                parser.popAndThrow("\"");
                String value = parser.pop();
                nestedParams.add((s) -> value);
                parser.popAndThrow("\"");
            }
            else if(parser.peek().equals("$")) {
                parser.popAndThrow("$");
                int paramNumber = Integer.parseInt(parser.pop());
                nestedParams.add((s) -> s[paramNumber-1]);
            }
            else {
                nestedParams.add(parseNestedFunctionCall(parser));
            }
        }
        parser.popAndThrow(")");

        return (s) -> {
          String[] array = new String[nestedParams.size()];
          for(int i = 0; i < nestedParams.size(); ++i)
              array[i] = nestedParams.get(i).apply(s);
          return parser.getResolvableComponent(function).getStringForm(array);
        };
    }


    /**
     * Parses the parameters and uses them to resolve
     * the expression into a single Regex expression.
     *
     * @param parser The Ogma parser with the tokens
     * @return A single Regex expression
     */
    @Override
    public String getStringForm(OgmaParser parser) {
        parser.popAndThrow(name);
        return getStringForm(parseParameters(parser));
    }


    /**
     * Takes the parameters uses them to resolve the expression
     * into a single Regex expression
     *
     * @param params The parameters passed to the component
     * @return The resulting Regex expression
     */
    @Override
    public String getStringForm(String[] params){
        if(params.length != numberOfArguments)
            throw new SyntaxErrorException(parser, "Expected " + numberOfArguments + " arguments. Got " + params.length);

        StringBuilder sb = new StringBuilder();
        for (Function<String[], String> func : components)
            sb.append(func.apply(params));

        return sb.toString();
    }


    /**
     * Returns the name of the function
     * @return The name of the function
     */
    @Override
    public String getName() {
        return name;
    }
}
