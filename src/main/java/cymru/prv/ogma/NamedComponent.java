package cymru.prv.ogma;

import java.util.function.Function;

/**
 * An interface to represent a named component.
 * All named components should be able to be
 * used for matching tokens and all of them
 * should have a unique identifier.
 *
 * @author Preben Vangberg
 * @since 1.0.0
 */
interface NamedComponent {

    /**
     * Creates a matcher based on the call to the
     * named component that is stored in the parser.
     *
     * Should return a function that takes a single
     * token and that returns a boolean whether it
     * matches or not.
     *
     * @param parser The parser where the call is stored
     * @return A function that can be used to match tokens
     */
    Function<String, Boolean> getMatcher(OgmaParser parser);


    /**
     * Should return the unique identifier of the object
     *
     * @return The name of the component.
     */
    String getName();

}
