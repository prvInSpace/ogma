package cymru.prv.ogma;


/**
 * Represents a target token in a search pattern.
 *
 * Functions the exact same way as a required token
 * but the brackets are normal parentheses.
 *
 * @see RequiredToken
 *
 * @author Preben Vangberg
 * @since 1.0.0
 */
class TargetToken implements TokenMatcher {

    private final Matcher matcher;

    /**
     * Parses the parentheses and the internal matcher
     * expression
     *
     * @param parser The current parser
     */
    TargetToken(OgmaParser parser){
        matcher = new ParenthesisMatcher(parser, "(", ")");
    }


    /**
     * Returns FOUND if the token matches the matcher
     * and MISMATCH otherwise
     *
     * @param word The word to search for
     * @return FOUND if token matches, MISMATCH otherwise
     */
    @Override
    public MatchType getMatchType(String word) {
        if(matcher.isMatch(word))
            return MatchType.FOUND;
        return MatchType.MISMATCH;
    }
}
