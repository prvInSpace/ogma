package cymru.prv.ogma;


/**
 * A special exception that can be thrown during the
 * parsing process that generates a uniform error message
 * with line number.
 *
 * @author Preben Vangberg
 * @since 1.0.0
 */
public class SyntaxErrorException extends RuntimeException {


    /**
     * Creates a new instance of the exception with the
     * data that is found in the parser and the error message
     *
     * @param parser The current parser that caused the exception
     * @param errorMessage The error message
     */
    SyntaxErrorException(OgmaParser parser, String errorMessage){
        super(String.format("Line %d: %s", parser.getLineNumber(), errorMessage));
    }

}
