package cymru.prv.ogma;

/**
 * Represent a component that is able to
 * match a single token.
 *
 * It is an abstract class because it has
 * the helper function parseMatcher which
 * is used to parse a series of matchers.
 *
 * @author Preben Vangberg
 * @since 1.0.0
 */
abstract class Matcher {

    /**
     * A function that is used to check single
     * tokens up against a matcher.
     *
     * Should return true if the token matches
     * the matcher and false otherwise.
     *
     * @param word The token to check
     * @return A boolean indicating if the token matches or not
     */
    public abstract boolean isMatch(String word);


    /**
     * Fetches a single matcher from the parser
     *
     * @param parser The parser to use
     * @return A single matcher
     */
    protected Matcher parseMatcher(OgmaParser parser){
        if ("\"".equals(parser.peek())) {
            return new RegexMatcher(parser);
        }
        else if("(".equals(parser.peek())) {
            return new ParenthesisMatcher(parser, "(", ")");
        }
        else if("!".equals(parser.peek())) {
            return new NotMatcher(parser);
        }
        else {
            return new NamedComponentMatcher(parser);
        }
    }

}
