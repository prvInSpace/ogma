package cymru.prv.ogma;

import java.util.regex.Pattern;


/**
 * A matcher made up of a single Regex expression
 *
 * @author Preben Vangberg
 * @since 1.0.0
 */
class RegexMatcher extends Matcher {

    private final Pattern pattern;

    /**
     * Parses the string literal and compiles it
     * into a Regex expression.
     *
     * @param parser The parser to use
     */
    RegexMatcher(OgmaParser parser){
        parser.popAndThrow("\"");
        pattern =  Pattern.compile(parser.pop());
        parser.popAndThrow("\"");
    }

    /**
     * Returns true if the pattern is
     * found within the given token.
     *
     * @param word The token to check
     * @return If the pattern could be found in the token
     */
    @Override
    public boolean isMatch(String word) {
        return pattern.matcher(word).find();
    }
}
