package cymru.prv.ogma;


/**
 * Represents a single parenthesis expression.
 *
 * Used by other parenthesis expressions as a standard
 * class for handling parsing.
 *
 * @author Preben Vangberg
 * @since 1.0.0
 */
class ParenthesisMatcher extends Matcher {

    private final Matcher matcher;


    /**
     * Parses a single parenthesis expression.
     *
     * @param parser The parser to use
     * @param start The opening parenthesis
     * @param end The closing parenthesis
     */
    ParenthesisMatcher(OgmaParser parser, String start, String end){
        parser.popAndThrow(start);

        Matcher leftSide = parseMatcher(parser);

        // Read until the closing parenthesis is found or we run out of tokens
        while((end == null && parser.hasNext()) || (end != null && !parser.peek().equals(end))){
            if(parser.peek().equals("|"))
                leftSide = new OrMatcher(parser, leftSide);
            else if(parser.peek().equals("&"))
                leftSide = new AndMatcher(parser, leftSide);
            else
                throw new SyntaxErrorException(parser, "Unexpected token " + parser.peek());
        }

        matcher = leftSide;

        // Special case when there is not closing parenthesis
        if(end != null)
            parser.popAndThrow(end);
    }

    /**
     * Returns the result of the expression within the parentheses
     *
     * @param word The token to check
     * @return The results of the expression inside the parentheses.
     */
    @Override
    public boolean isMatch(String word) {
        return matcher.isMatch(word);
    }
}
