package cymru.prv.ogma;

/**
 * An interface for named components
 * that are able to be determined (resolved)
 * before runtime.
 *
 * This means that they are able to be
 * converted into a Regex strings literal.
 *
 * @author Preben Vangberg
 * @since 1.0.0
 */
public interface Resolvable {

    /**
     * Takes a parser and parses a call to the named
     * component.
     *
     * @param parser The Ogma parser with the tokens
     * @return A Regex expression
     */
    String getStringForm(OgmaParser parser);


    /**
     * Takes a set of parameters and uses them to
     * create a Regex expression. Used for nested
     * calls where the parsing of the component has
     * already been taken care of by another component.
     *
     * @param params The parameters passed to the component
     * @return A singular Regex expression
     */
    String getStringForm(String[] params);

}
