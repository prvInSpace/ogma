package cymru.prv.ogma;

import java.util.LinkedList;
import java.util.List;

/**
 * Contains information about a match for a search pattern.
 */
public class Match {

    private final String target;
    private final int targetIndex;
    private final List<String> words;
    private final List<String> fullSequence;
    private final SearchPattern pattern;

    Match(SearchPattern pattern, List<String> words, String target, int targetIndex, List<String> fullSequence){
        this.pattern = pattern;
        this.words = words;
        this.target = target;
        this.targetIndex = targetIndex;
        this.fullSequence = fullSequence;
    }

    /**
     * Returns the target word as defined by the
     * search pattern.
     *
     * @return the target word
     */
    public String getTarget(){
        return target;
    }


    /**
     * Returns the index of the target word in
     * the full sequence of words that was searched
     * in.
     *
     * @return the index of the target word
     */
    public int getTargetIndex(){
        return targetIndex;
    }

    /**
     * Returns all the words in the match as defined
     * by the search pattern.
     *
     * @return all the words in the match.
     */
    public List<String> getWords(){
        return new LinkedList<>(words);
    }

    /**
     * Returns a list of all the groups of the
     * search pattern.
     *
     * @return the groups of the search pattern.
     */
    public List<String> getGroups() {
        return new LinkedList<>(pattern.getGroups());
    }

    /**
     * The full text sequence in which the match
     * was found.
     *
     * @return the full text sequence
     */
    public List<String> getFullSequence(){
        return new LinkedList<>(fullSequence);
    }

    /**
     * Returns the identifier / name of the search
     * pattern that gave rise to the match.
     *
     * @return the name of the pattern.
     */
    public String getSearchPatternName(){
        return pattern.getName();
    }
}
