package cymru.prv.ogma;

import java.util.function.Function;


/**
 * Represents a single external regex function
 * An external Regex function is a resolvable function
 * that returns a single Regex expression given a set
 * of parameters.
 *
 * @author Preben Vangberg
 * @since 1.0.0
 */
class ExternalRegexFunction extends ResolvableFunction {

    private final String name;
    private final Function<String[], String> function;


    /**
     * Creates an external regex function based on
     * the name of the function and a pointer to
     * the external function,
     *
     * @param name The name of the function
     * @param function The pointer to the external function
     */
    ExternalRegexFunction(String name, Function<String[], String> function){
        this.name = name;
        this.function = function;
    }

    /**
     * Takes the parser and parses a call to the function.
     * This call should consist of the name of the component, then a (, followed by a comma
     * separated list of parameters, and a ).
     *
     * Since this is an external function these are sent
     * of the function defined in the constructor.
     *
     * @param parser Takes an instance of OgmaParser
     * @return The string literal generated from the function call.
     */
    @Override
    public String getStringForm(OgmaParser parser) {
        parser.popAndThrow(name);
        return function.apply(parseParameters(parser));
    }


    /**
     * Returns the result from the external function
     *
     * @param params The parameters passed to the component
     * @return A regex expression from the external function
     */
    @Override
    public String getStringForm(String[] params) {
        return function.apply(params);
    }


    /**
     * The name of the component
     * @return The name of the component
     */
    @Override
    public String getName() {
        return name;
    }
}
