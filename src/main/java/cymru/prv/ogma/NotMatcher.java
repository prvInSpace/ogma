package cymru.prv.ogma;


/**
 * Represents a NOT operator in Ogma
 *
 * @author Preben Vangberg
 * @since 1.0.0
 */
class NotMatcher extends Matcher {

    private final Matcher matcher;

    /**
     * Parses the operator and the subsequent
     * matcher expression
     *
     * @param parser The parser to use
     */
    NotMatcher(OgmaParser parser){
        parser.popAndThrow("!");
        matcher = parseMatcher(parser);
    }

    /**
     * Returns the negated result of the
     * internal matcher.
     *
     * @param word The token to check
     * @return The negated result from the matcher.
     */
    @Override
    public boolean isMatch(String word) {
        return !matcher.isMatch(word);
    }
}
