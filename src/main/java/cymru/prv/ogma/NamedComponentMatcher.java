package cymru.prv.ogma;

import java.util.function.Function;


/**
 * Represents a named component matcher
 * Will match words up against any named
 * component.
 *
 * @author Preben Vangberg
 * @since 1.0.0
 */
class NamedComponentMatcher extends Matcher {

    // The matcher from the named component
    private final Function<String, Boolean> matcher;


    /**
     * Parses a call to the name component and fetches
     * the matcher function that results from that call.
     *
     * @param parser The parser to use
     */
    NamedComponentMatcher(OgmaParser parser){
        matcher = parser.getNamedComponent(parser.peek()).getMatcher(parser);
    }


    /**
     * Returns the value of the matcher
     * function.
     *
     * @param word The token to check
     * @return A boolean indicating whether the word matches or not
     */
    @Override
    public boolean isMatch(String word) {
        return matcher.apply(word);
    }
}
