package cymru.prv.ogma;

/**
 * Represent a required token
 *
 * @author Preben Vangberg
 * @since 1.0.0
 */
class RequiredToken implements TokenMatcher {

    private final Matcher matcher;

    /**
     * Parses the parentheses and the internal
     * matcher expression.
     *
     * @param parser The current parser
     */
    RequiredToken(OgmaParser parser){
        matcher = new ParenthesisMatcher(parser, "{", "}");
    }


    /**
     * Returns found if the match was found and
     * mismatch otherwise.
     *
     * @param word The word to check
     * @return FOUND if the word matches, MISMATCH otherwise
     */
    @Override
    public MatchType getMatchType(String word) {
        if(matcher.isMatch(word))
            return MatchType.FOUND;
        return MatchType.MISMATCH;
    }
}
