package cymru.prv.ogma;

/**
 * An interface representing classes able to
 * match individual tokens.
 *
 * @see RequiredToken
 * @see TargetToken
 * @see OptionalToken
 *
 * @author Preben Vangberg
 * @since 1.0.0
 */
interface TokenMatcher {


    /**
     * Checks whether the given word
     * matches the token matcher or not.
     *
     * @param word The word to check
     * @return The resulting match type
     */
    MatchType getMatchType(String word);

}
