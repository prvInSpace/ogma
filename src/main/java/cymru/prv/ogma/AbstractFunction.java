package cymru.prv.ogma;

import java.util.LinkedList;
import java.util.List;

/**
 * Represents a function in the general sense.
 * This includes both internal and external
 * Regex functions and external matcher functions.
 *
 * @author Preben Vangberg
 * @since 1.0.0
 */
abstract class AbstractFunction implements NamedComponent {


    /**
     * Helper function that helps with parsing
     * function call parameters.
     *
     * @param parser The current parser
     * @return The function parameters as a string array
     */
    protected String[] parseParameters(OgmaParser parser){
        List<String> params = new LinkedList<>();

        parser.popAndThrow("(");

        boolean first = true;
        while(!parser.peek().equals(")")){

            // If it is not the first parameter we should expect
            // A comma to separate the parameters.
            if(!first)
                parser.popAndThrow(",");
            else
                first = false;

            if ("\"".equals(parser.peek())) {
                parser.popAndThrow("\"");
                params.add(parser.pop());
                parser.popAndThrow("\"");
            } else {
                params.add(parser.parseResolvableComponent());
            }
        }

        parser.popAndThrow(")");

        return params.toArray(String[]::new);
    }
}
