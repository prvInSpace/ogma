package cymru.prv.ogma;

import java.io.Reader;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Scanner;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * An object used to store information about the
 * current parsing. Contains the token queue,
 * information about line numbers, and similar.
 *
 * Should never really be created outside this class
 * or in tests.
 *
 * @author Preben Vangberg
 * @since 1.0.0
 */
class OgmaParser {

    private final Ogma ogma;
    private Queue<String> tokens = new LinkedBlockingQueue<>();
    private boolean inMultilineComment = false;
    private final List<String> groups = new LinkedList<>();
    private String currentLine = "";
    private int lineNumber = 0;


    /**
     * Creates an instance of the Parser with the given
     * Ogma instance.
     *
     * @param ogma The Ogma instance to add the content to
     */
    private OgmaParser(Ogma ogma){
        this.ogma = ogma;
    }


    /**
     * Creates a simple Ogma parser based on a single line
     *
     * @implNote Used for testing
     *
     * @param line The line to parse
     */
    OgmaParser(String line){
        this(new Ogma(), line);
    }


    /**
     * Creates an instance of the Ogma parser
     * based on an Ogma instance
     *
     * @implNote Used for testing
     *
     * @param ogma The Ogma instance to add the content to
     * @param line The line to parse
     */
    OgmaParser(Ogma ogma, String line){
        this.ogma = ogma;
        currentLine = line.trim();
        tokens = tokenize(line);
    }


    /**
     * Pops a single token from the queue.
     *
     * @throws SyntaxErrorException If the queue is empty
     * @return A single string token
     */
    public String pop(){
        if(tokens.isEmpty())
            throw new SyntaxErrorException(this, "Unexpected end of line.");
        return tokens.remove();
    }


    /**
     * Pop a single token and verify that the token
     * is the same as the expected token in the process.
     *
     * @throws SyntaxErrorException if the expected token does not match the next token
     * @param expected The expected token
     * @return A single string from the queue
     */
    public String popAndThrow(String expected){
        String value = pop();
        if(!value.equals(expected))
            throw new SyntaxErrorException(this, "Expected token '" + expected +"'. Got '" + value + "'");
        return value;
    }


    /**
     * Fetches the next token without removing
     * it from the queue.
     *
     * @return The next string in the queue
     */
    public String peek(){
        return tokens.peek();
    }


    /**
     * Checks if the queue has more tokens
     * in it.
     *
     * @return A boolean indicating wheter the queue is empty or not
     */
    public boolean hasNext(){
        return !tokens.isEmpty();
    }


    /**
     * Parses a single line expression
     */
    private void parse(){
        // Confirm that the line is not empty
        if(tokens.isEmpty())
            return;

        // Handle multi line comments
        if(tokens.peek().equals("%")){
            tokens.remove();
            if(!tokens.isEmpty() && tokens.peek().equals("%")) {
                inMultilineComment = !inMultilineComment;
                tokens.remove();

                // If we just finished a multiline comment and there are more
                // tokens we should throw an exception.
                if (!inMultilineComment && !tokens.isEmpty())
                    throw new SyntaxErrorException(this, "Unexpected token '" + tokens.peek() + "'");
            }
            return;
        }
        else if(inMultilineComment)
            return;

        // Choose what type of expression we are looking at
        switch(tokens.peek()){
            case "group":
                popAndThrow("group");
                groups.add(pop());
                break;
            case "end":
                popAndThrow("end");
                if(groups.size() == 0)
                    throw new SyntaxErrorException(this, "Unexpect end of group. No group exist");
                groups.remove(groups.size()-1);
                break;
            case "matcher":
                ogma.addNamedComponent(new NamedMatcher(this), this);
                break;
            case "const":
                ogma.addNamedComponent(new Constant(this), this);
                break;
            case "function":
                ogma.addNamedComponent(new InternalRegexFunction(this), this);
                break;
            default:
                ogma.addSearchPattern(new SearchPattern(this));
        }

        if(!tokens.isEmpty())
            throw new SyntaxErrorException(this, "Unexpected token " + tokens.peek() + ". Expected newline");
    }


    /**
     * Wraps the reader in a scanner, then sends it
     * off to be parsed.
     *
     * @param ogma The Ogma instance to add the content to
     * @param reader The reader where the Ogma code can be read from
     */
    static void parse(Ogma ogma, Reader reader){
        parse(ogma, new Scanner(reader));
    }


    /**
     * Parsers the content of the reader
     * and adds the content of that file
     * to the Ogma instance provided
     *
     * @throws SyntaxErrorException if a syntax error is encountered
     * @param ogma the ogma instance which the content should be added to
     * @param reader the reader containing the Ogma code
     */
    static void parse(Ogma ogma, Scanner reader){
        OgmaParser parser = new OgmaParser(ogma);
        while (reader.hasNext()) {
            var line = reader.nextLine();
            parser.currentLine = line.trim();
            parser.lineNumber++;
            parser.tokens = parser.tokenize(line);
            parser.parse();
        }
        if(parser.groups.size() != 0)
            throw new SyntaxErrorException(parser, "Group " + parser.groups.get(0) + " not closed");
    }


    /**
     * Tokenizes the string.
     *
     * Creates a queue with all of the identifiers, strings,
     * and symbols separated from each-other.
     *
     * if a double quote is entered it will read the text until it
     * reaches another double quote.
     *
     * @param line The line that should be tokenized
     * @return A queue with all the tokens in the line.
     */
    Queue<String> tokenize(String line){
        Queue<String> tokens = new LinkedBlockingQueue<>();

        int start = 0;
        int i;
        for(i = 0; i < line.length(); ++i){
            char c = line.charAt(i);

            if(start == i && "\t\n ".contains("" + c)){
                start++;
            }

            // If the string contains one of these letters then
            // cut the string and add the char as a token
            else if("{}[](),=&|!$%\" \t\n".contains("" +c)){

                // If we have some string in the buffer
                if(start != i){
                    tokens.add(line.substring(start, i));
                }
                // Add the token itself
                if(!" \t\n".contains("" + c))
                    tokens.add("" + c);

                // Restart the start counter
                start = i + 1;

                // If the character is a double quote we need to parse the
                // String until we find the end quote.
                if(c == '"'){
                    i++;
                    while(i < line.length() && line.charAt(i) != '"')
                        i++;

                    if(i == line.length())
                        throw new SyntaxErrorException(this, "Unclosed String");

                    tokens.add(line.substring(start, i));

                    tokens.add("\"");

                    // Restart the start counter
                    start = i + 1;
                }
            }
        }

        if(start != i)
            tokens.add(line.substring(start));


        return tokens;
    }


    /**
     * Parses and returns the resolved version
     * of a resolvable component
     *
     * @return The Regex expression resulting from the call
     */
    public String parseResolvableComponent(){
        String name = peek();
        Resolvable component = getResolvableComponent(name);
        return component.getStringForm(this);
    }


    /**
     * Fetches the named component with the give name.
     *
     * @throws SyntaxErrorException If component does not exist
     * @param name The name of the component
     * @return The component with the given name
     */
    public NamedComponent getNamedComponent(String name){
        NamedComponent component = ogma.getNamedComponent(name);
        if(component == null)
            throw new SyntaxErrorException(this, "Unknown component '" + name + "'");
        return component;
    }


    /**
     * Fetch a resolvable component
     *
     * @throws SyntaxErrorException If the component does not exist
     * @param name The name of the component
     * @return The component wuth the given name
     */
    public Resolvable getResolvableComponent(String name){
        NamedComponent component = getNamedComponent(name);
        if(!(component instanceof Resolvable))
            throw new SyntaxErrorException(this, "Component '" + name + "' is not resolvable.");
        return (Resolvable) component;
    }


    /**
     * Returns a copy of the list of groups
     * that is currently active.
     *
     * @return A list of currently active groups
     */
    public List<String> getCurrentGroups(){
        return new LinkedList<>(groups);
    }


    /**
     * Returns the string form of the current line
     * @return The current line
     */
    public String getCurrentLine() {
        return currentLine;
    }


    /**
     * Fetches the current line number
     * @return The current line number
     */
    public int getLineNumber() {
        return lineNumber;
    }
}
