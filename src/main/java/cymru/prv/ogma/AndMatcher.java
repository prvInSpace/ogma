package cymru.prv.ogma;


/**
 * Represents an AND operator
 *
 * @author Preben Vangberg
 * @since 1.0.0
 */
class AndMatcher extends Matcher {

    // The two matchers on either side
    // of the operator
    private final Matcher leftSide;
    private final Matcher rightSide;

    /**
     * Parses the right side of the operator
     * and combines them into a single expression
     *
     * @param parser The parser
     * @param leftSide The left side of the operator
     */
    AndMatcher(OgmaParser parser, Matcher leftSide){
        this.leftSide = leftSide;
        parser.popAndThrow("&");
        rightSide = parseMatcher(parser);
    }

    /**
     * Returns true if both sides are true.
     * Otherwise false.
     *
     * @param word The word to match
     * @return True only if both sides are true
     */
    @Override
    public boolean isMatch(String word) {
        return leftSide.isMatch(word) && rightSide.isMatch(word);
    }
}
