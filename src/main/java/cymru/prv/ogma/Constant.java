package cymru.prv.ogma;

import java.util.function.Function;
import java.util.regex.Pattern;

/**
 * Represents a named Regex constant
 *
 * @author Preben Vangberg
 * @since 1.0.0
 */
class Constant implements NamedComponent, Resolvable {

    private final String literal;
    private final String name;


    /**
     * Creates a constant based on the given parameters
     *
     * @param name Of the constant
     * @param literal The Regex expression
     */
    public Constant(String name, String literal){
        this.name = name;
        this.literal = literal;
    }


    /**
     * Parses the constant and all of the components of
     * the constant expression.
     *
     * @param parser The parser where the definition is stored
     */
    Constant(OgmaParser parser){
        parser.popAndThrow("const");
        name = parser.pop();
        parser.popAndThrow("=");

        StringBuilder sb = new StringBuilder();
        while(parser.hasNext()){
            if(parser.peek().equals("\"")) {
                parser.popAndThrow("\"");
                sb.append(parser.pop());
                parser.popAndThrow("\"");
            }
            else
                sb.append(parser.parseResolvableComponent());
        }

        literal = sb.toString();
    }


    /**
     * Compiles a Regex pattern based on the pattern
     * and returns a lambda call to this matcher.
     *
     * The matcher should return true if the pattern
     * can be found within the token.
     *
     * @param parser The parser where the call is stored
     * @return A lambda to a compiled Regex pattern.
     */
    @Override
    public Function<String, Boolean> getMatcher(OgmaParser parser) {
        parser.popAndThrow(name);
        Pattern pattern = Pattern.compile(literal);
        return s -> pattern.matcher(s).find();
    }


    /**
     * Returns the content of the constant
     *
     * @param parser The Ogma parser with the tokens
     * @return The content of the constant
     */
    @Override
    public String getStringForm(OgmaParser parser) {
        parser.popAndThrow(name);
        return literal;
    }


    /**
     * Returns the content of the constant
     *
     * @param params The parameters passed to the component
     * @return The content of the constant
     */
    @Override
    public String getStringForm(String[] params) {
        return literal;
    }


    /**
     * Returns the name of the constant
     * @return The name of the constant
     */
    @Override
    public String getName() {
        return name;
    }
}
