package cymru.prv.ogma;

import cymru.prv.ogma.MatchType;
import cymru.prv.ogma.OgmaParser;
import cymru.prv.ogma.OptionalToken;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TestOptionalToken {

    @Test
    public void testEmptyToken(){
        Assertions.assertThrows(
                SyntaxErrorException.class,
                () -> new OptionalToken(new OgmaParser("[]"))
        );
    }

    @Test
    public void testValidToken(){
        OptionalToken token = new OptionalToken(new OgmaParser("[\"hello\"]"));
        Assertions.assertEquals(MatchType.FOUND, token.getMatchType("hello"));
        Assertions.assertEquals(MatchType.OPTIONAL_MISMATCH, token.getMatchType("world"));
    }

}
