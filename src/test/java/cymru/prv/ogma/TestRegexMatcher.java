package cymru.prv.ogma;

import cymru.prv.ogma.OgmaParser;
import cymru.prv.ogma.RegexMatcher;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TestRegexMatcher {

    @Test
    public void testRegexMatcher(){
        RegexMatcher matcher = new RegexMatcher(new OgmaParser("\"hello\""));
        Assertions.assertTrue(matcher.isMatch("hello"));
    }

}
