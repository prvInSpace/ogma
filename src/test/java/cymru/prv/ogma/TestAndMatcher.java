package cymru.prv.ogma;

import cymru.prv.ogma.AndMatcher;
import cymru.prv.ogma.Matcher;
import cymru.prv.ogma.OgmaParser;
import cymru.prv.ogma.RegexMatcher;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TestAndMatcher {

    @Test
    public void testValidAndMatcher(){
        Matcher and = new AndMatcher(
                new OgmaParser("&\"world\""),
                new RegexMatcher(new OgmaParser("\"hello\""))
        );
        Assertions.assertFalse(and.isMatch("hello"));
        Assertions.assertFalse(and.isMatch("world"));
        Assertions.assertTrue(and.isMatch("hello, world!"));
    }

}
