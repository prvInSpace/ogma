package cymru.prv.ogma;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TestNamedComponentMatcher {

    @Test
    public void testValidConstantMatcher(){
        Ogma ogma = new Ogma();
        ogma.addStringLiteral("test", "hello");

        Assertions.assertTrue(new NamedComponentMatcher(new OgmaParser(ogma,"test")).isMatch("hello"));

    }

    @Test
    public void testFunctionMatcher(){
        Ogma ogma = new Ogma();
        ogma.addNamedComponent(new ExternalRegexFunction("test", s -> s[0]));
        OgmaParser call = new OgmaParser(ogma, "test(\"hello\")");
        Assertions.assertTrue(new NamedComponentMatcher(call).isMatch("hello"));
    }

    @Test
    public void testMissingConstant(){
        Assertions.assertThrows(
                SyntaxErrorException.class,
                () -> new NamedComponentMatcher(new OgmaParser("s(\"hello\")"))
        );
    }

}
