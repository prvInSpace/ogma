package cymru.prv.ogma;

import cymru.prv.ogma.MatchType;
import cymru.prv.ogma.OgmaParser;
import cymru.prv.ogma.RequiredToken;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TestRequiredToken {

    @Test
    public void testEmptyToken(){
        Assertions.assertThrows(
                SyntaxErrorException.class,
                () -> new RequiredToken(new OgmaParser("{}"))
        );
    }

    @Test
    public void testValidToken(){
        RequiredToken token = new RequiredToken(new OgmaParser("{\"hello\"}"));
        Assertions.assertEquals(MatchType.FOUND, token.getMatchType("hello"));
        Assertions.assertEquals(MatchType.MISMATCH, token.getMatchType("world"));
    }

}
