package cymru.prv.ogma;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.*;
import java.nio.Buffer;
import java.util.List;
import java.util.Queue;

public class TestOgmaParser {


    @Test
    public void testPopAndThrowExpected(){
        Assertions.assertEquals(
                "(",
                new OgmaParser("(").popAndThrow("(")
        );
    }

    @Test
    public void testPopAndThrowUnexpected(){
        Assertions.assertThrows(
                SyntaxErrorException.class,
                () -> new OgmaParser("(").popAndThrow(")")
        );
    }

    /*
            TESTS FOR TOKENIZER
     */
    @Test
    public void testEmptyString(){
        Queue<String> tokens = new OgmaParser("").tokenize("");
        Assertions.assertEquals(0, tokens.size());
    }

    @Test
    public void testBrackets(){
        Queue<String> tokens = new OgmaParser("").tokenize("{([])}");
        Assertions.assertIterableEquals(List.of("{", "(", "[", "]", ")", "}"), tokens);
    }

    @Test
    public void testWhiteSpaceInString(){
        Queue<String> tokens = new OgmaParser("").tokenize("( )");
        Assertions.assertIterableEquals(List.of("(", ")"), tokens);
    }

    @Test
    public void testFunctionCall(){
        Queue<String> tokens = new OgmaParser("").tokenize("{ myFunction() }");
        Assertions.assertIterableEquals(List.of("{", "myFunction", "(", ")", "}"), tokens);
    }

    @Test
    public void testStringLiteral(){
        Queue<String> tokens = new OgmaParser("").tokenize("\"hello\"");
        Assertions.assertIterableEquals(List.of("\"", "hello", "\""), tokens);
    }

    @Test
    public void testUnclosedString(){
        Assertions.assertThrows(SyntaxErrorException.class, () -> new OgmaParser("").tokenize("\"hello"));
    }

    @Test
    public void testWordsSeparatedBySpace(){
        Queue<String> tokens = new OgmaParser("").tokenize("hello world");
        Assertions.assertIterableEquals(List.of("hello", "world"), tokens);
    }

    @Test
    public void testLogicalOperators(){
        Queue<String> tokens = new OgmaParser("").tokenize("&=!|");
        Assertions.assertIterableEquals(List.of("&", "=", "!", "|"), tokens);
    }

    @Test
    public void testConstantDeclaration(){
        Queue<String> tokens = new OgmaParser("").tokenize("const myConst=\"hello, world!\"");
        Assertions.assertIterableEquals(List.of("const", "myConst", "=", "\"", "hello, world!", "\""), tokens);
    }

    @Test
    public void testFunctionDeclaration(){
        Queue<String> tokens = new OgmaParser("").tokenize("function myFunc(2) = myConst $1 ");
        Assertions.assertIterableEquals(
                List.of("function", "myFunc", "(", "2", ")", "=", "myConst", "$", "1"),
                tokens
        );
    }

    @Test
    public void testEmptyStringLiteral(){
        Queue<String> tokens = new OgmaParser("").tokenize("\"\"");
        Assertions.assertIterableEquals(List.of("\"", "", "\""), tokens);
    }

    @Test
    public void testCallToUndeclaredFunctionShouldThrowException(){
        Assertions.assertThrows(
                SyntaxErrorException.class,
                () -> new Constant(new OgmaParser("const test = s(\"hello\")"))
        );
    }

    private BufferedReader wrapString(String s){
        return new BufferedReader(new StringReader(s));
    }

    @Test
    public void testEmptyFile(){
        Assertions.assertDoesNotThrow(
                () -> OgmaParser.parse(new Ogma(), wrapString(""))
        );
    }

    @Test
    public void testAddConstant(){
        Ogma ogma = new Ogma();
        OgmaParser.parse(ogma, wrapString("const myConst = \"test\""));
        Constant myConst = (Constant) ogma.getNamedComponent("myConst");
        Assertions.assertNotNull(myConst);
        Assertions.assertEquals("myConst", myConst.getName());
        Assertions.assertEquals("test", myConst.getStringForm(new String[0]));
    }

    @Test
    public void testAddFunction(){
        Ogma ogma = new Ogma();
        OgmaParser.parse(ogma, wrapString("function func(1) = $1"));
        ResolvableFunction myConst = (ResolvableFunction) ogma.getNamedComponent("func");
        Assertions.assertNotNull(myConst);
        Assertions.assertEquals("func", myConst.getName());
        Assertions.assertEquals("test", myConst.getStringForm(new String[]{"test"}));
    }

    @Test
    public void testAddingTwoConstants() {
        Ogma ogma = new Ogma();
        OgmaParser.parse(ogma, wrapString(
                "const myConst = \"hello\"\n" +
                        "const myConst2 = \"world\""));

        Constant myConst = (Constant) ogma.getNamedComponent("myConst");
        Assertions.assertNotNull(myConst);
        Assertions.assertEquals("myConst", myConst.getName());
        Assertions.assertEquals("hello", myConst.getStringForm(new String[0]));

        Constant myConst2 = (Constant) ogma.getNamedComponent("myConst2");
        Assertions.assertNotNull(myConst2);
        Assertions.assertEquals("myConst2", myConst2.getName());
        Assertions.assertEquals("world", myConst2.getStringForm(new String[0]));
    }

    @Test
    public void testAddSearchPattern() {
        Assertions.assertDoesNotThrow(
                () -> OgmaParser.parse(new Ogma(), wrapString(
                  "\n\n{\"hello\"}(\"world\")"
                ))
        );
    }

    @Test
    public void testParsingASingleLineComment(){
        Assertions.assertDoesNotThrow(
                () -> OgmaParser.parse(new Ogma(), wrapString("% Hello, World!"))
        );
    }

    @Test
    public void testParsingSingleLineCommentWithoutAnythingAfterwards(){
        Assertions.assertDoesNotThrow(
                () -> OgmaParser.parse(new Ogma(), wrapString("%"))
        );
    }

    @Test
    public void testParsingSingleLineCommentWithoutSpace(){
        Assertions.assertDoesNotThrow(
                () -> OgmaParser.parse(new Ogma(), wrapString("%Hello, World!"))
        );
    }

    @Test
    public void testParsingMultilineComment(){
        Assertions.assertDoesNotThrow(
                () -> OgmaParser.parse(new Ogma(), wrapString("%%Hello\nWorld!\n%% "))
        );
    }

    @Test
    public void testMultilineCommentStartOnStatementLine(){
        Assertions.assertThrows(SyntaxErrorException.class, (
                () -> OgmaParser.parse(new Ogma(),
                        wrapString("const hello = \"Hello\" %%"
                        )
                ))
        );
    }

    @Test
    public void testThrowsIfTokensAfterEndOfMultiLineComment(){
        Assertions.assertThrows(SyntaxErrorException.class, () ->
                OgmaParser.parse(new Ogma(), wrapString(
                        "%%\nhello\n%% const test = \"Test\""
                ))
        );
    }

    @Test
    public void testAddAndRemoveGroup(){
        Assertions.assertDoesNotThrow(
                () -> OgmaParser.parse(new Ogma(),
                        wrapString("group Hello\nend"))
        );
    }

    @Test
    public void testRemoveGroupIfEmpty(){
        Assertions.assertThrows(
                SyntaxErrorException.class,
                () -> OgmaParser.parse(new Ogma(), wrapString("end"))
        );
    }

    private BufferedReader wrap(List<String> lines){
        return new BufferedReader(new StringReader(String.join("\n", lines)));
    }

    @Test
    public void testAddGroupNoToken(){
        Assertions.assertThrows(
                SyntaxErrorException.class,
                () -> OgmaParser.parse(new Ogma(), wrapString("group"))
        );
    }

    @Test
    public void testAddGroupTooManyToken(){
        Assertions.assertThrows(
                SyntaxErrorException.class,
                () -> OgmaParser.parse(new Ogma(), wrapString("group Hello World"))
        );
    }

    @Test
    public void testShouldThrowIfGroupsNotEnded(){
        Assertions.assertThrows(
                SyntaxErrorException.class,
                () -> OgmaParser.parse(new Ogma(), wrapString("group Hello"))
        );
    }

    @Test
    public void testSearchPatternInGroup() throws IOException {
        Ogma ogma = new Ogma();
        List<String> program = List.of(
                "group test",
                "(\"hello\")",
                "end"
        );

        ogma.parseOgmaFile(wrap(program));

        ogma.findMatches(new TextLineReader(wrapString("hello")), (m) -> {
            Assertions.assertEquals("hello",m.getWords().get(0));
            Assertions.assertIterableEquals(List.of("test"), m.getGroups());
        });
    }

    @Test
    public void testParseNamedMatcher(){
        Assertions.assertDoesNotThrow(
                () -> OgmaParser.parse(new Ogma(), wrapString("matcher test = \"hello\""))
        );
    }

}
