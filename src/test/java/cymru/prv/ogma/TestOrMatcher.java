package cymru.prv.ogma;

import cymru.prv.ogma.Matcher;
import cymru.prv.ogma.OgmaParser;
import cymru.prv.ogma.OrMatcher;
import cymru.prv.ogma.RegexMatcher;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TestOrMatcher {

    @Test
    public void testValidOrMatcher(){
        Matcher leftSide = new RegexMatcher(new OgmaParser("\"hello\""));
        Matcher or = new OrMatcher(new OgmaParser("|\"world\""), leftSide);

        Assertions.assertTrue(or.isMatch("hello"));
        Assertions.assertTrue(or.isMatch("world"));
        Assertions.assertFalse(or.isMatch("test"));
    }

}
