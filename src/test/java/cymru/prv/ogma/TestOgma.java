package cymru.prv.ogma;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.util.List;
import java.util.Scanner;

public class TestOgma {

    @Test
    public void testAddExternalFunction(){
        Ogma ogma = new Ogma();
        ogma.addRegexFunction("test", (s) -> s[0]);
        Resolvable comp = (Resolvable) ogma.getNamedComponent("test");
        Assertions.assertNotNull(comp);
        Assertions.assertEquals(
                "hello",
                comp.getStringForm(new String[]{"hello"})
        );
    }

    @Test
    public void testAddRuntimeFunction(){
        Ogma ogma = new Ogma();
        ogma.addMatcherFunction("test", (s, t) -> s.matches(t[0]));
        NamedComponent comp = ogma.getNamedComponent("test");
        Assertions.assertNotNull(comp);
        Assertions.assertTrue(comp instanceof ExternalMatcherFunction);
        Assertions.assertTrue(
                comp.getMatcher(new OgmaParser("test(\"hello\")"))
                    .apply("hello")
        );
    }

    private Reader wrap(List<String> lines){
        return new StringReader(String.join("\n", lines));
    }

    @Test
    public void testParseFile() throws IOException {
        Ogma ogma = new Ogma();
        List<String> program = List.of(
                "const const1 = \"hello\"",
                "function func(1) = $1",
                "{const1}[\",\"](func(\"world\"))"
        );
        List<String> text = List.of(
                "hello , world",
                "something, world",
                "hello, world!"
        );

        Assertions.assertDoesNotThrow(() ->
                ogma.parseOgmaFile(wrap(program))
        );

        TextReader reader = new TextLineReader(new BufferedReader(wrap(text)));

        class Inner {
            int matches = 0;
            void handleMatch(Match match){
                if(matches == 0)
                    Assertions.assertIterableEquals(
                            List.of("hello", ",", "world"),
                            match.getWords());
                else
                    Assertions.assertIterableEquals(
                            List.of("hello,", "world!"),
                            match.getWords());
                matches++;
            }
        }
        Inner inner = new Inner();

        ogma.findMatches(reader, inner::handleMatch);
        Assertions.assertEquals(2, inner.matches);
    }


    @Test
    public void testCreatingADuplicateNamedComponentShouldThrow(){
        List<String> code = List.of(
                "const hello = \"something\"",
                "const hello = \"world\""
        );
        Assertions.assertThrows(SyntaxErrorException.class, () -> {
            Ogma ogma = new Ogma();
            OgmaParser.parse(ogma, new StringReader(String.join("\n", code)));
        });
    }

    @Test
    public void testShouldBeAbleToUseScanner(){
        Assertions.assertDoesNotThrow(() -> {
            Ogma ogma = new Ogma();
            Scanner scanner = new Scanner(new StringReader(""));
            ogma.parseOgmaFile(scanner);
        });
    }
}
