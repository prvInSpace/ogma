package cymru.prv.ogma;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TestTargetToken {

    @Test
    public void testEmptyToken(){
        Assertions.assertThrows(
                SyntaxErrorException.class,
                () -> new TargetToken(new OgmaParser("()"))
        );
    }

    @Test
    public void testValidToken(){
        TargetToken token = new TargetToken(new OgmaParser("(\"hello\")"));
        Assertions.assertEquals(MatchType.FOUND, token.getMatchType("hello"));
        Assertions.assertEquals(MatchType.MISMATCH, token.getMatchType("world"));
    }

}
