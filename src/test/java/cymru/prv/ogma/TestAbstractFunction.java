package cymru.prv.ogma;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TestAbstractFunction {

    @Test
    public void testParseWithNoParameters(){
        ExternalRegexFunction function = new ExternalRegexFunction("test", (s) -> null);
        Assertions.assertArrayEquals(new String[0], function.parseParameters(new OgmaParser("()")));
    }

    @Test
    public void testParseWith1Parameter(){
        ExternalRegexFunction function = new ExternalRegexFunction("test", (s) -> null);
        OgmaParser parser = new OgmaParser("(\"test\")");
        Assertions.assertArrayEquals(new String[]{"test"}, function.parseParameters(parser));
    }

    @Test
    public void testParseWith2Parameters(){
        ExternalRegexFunction function = new ExternalRegexFunction("test", (s) -> s[0] + s[1]);
        OgmaParser parser = new OgmaParser("(\"test\",\"hello\")");
        Assertions.assertArrayEquals(new String[]{"test", "hello"}, function.parseParameters(parser));
    }

    @Test
    public void testParseWithNamedConstant(){
        Ogma ogma = new Ogma();
        ogma.addNamedComponent(new Constant(new OgmaParser("const test = \"testString\"")));

        ExternalRegexFunction function = new ExternalRegexFunction("test2", (s) -> null);
        Assertions.assertArrayEquals(
                new String[]{"testString"},
                function.parseParameters(new OgmaParser(ogma, "(test)"))
        );
    }

}
