package cymru.prv.ogma;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TestNotMatcher {

    @Test
    public void testValidNotMatcher(){
        Matcher not = new NotMatcher(new OgmaParser("!\"hello\""));
        Assertions.assertFalse(not.isMatch("hello"));
        Assertions.assertTrue(not.isMatch("world"));
    }

}
