package cymru.prv.ogma;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TestInternalRegexFunction {

    @Test
    public void testEmptyFunction(){
        InternalRegexFunction function = new InternalRegexFunction(new OgmaParser("function test() = "));
        Assertions.assertEquals("test", function.getName());
        Assertions.assertEquals("", function.getStringForm(new OgmaParser("test()")));
    }

    @Test
    public void testFunctionWith1ConstantArgument(){
        InternalRegexFunction function = new InternalRegexFunction(new OgmaParser("function test(1) = $1"));
        Assertions.assertEquals("hello", function.getStringForm(new OgmaParser("test(\"hello\")")));
    }

    @Test
    public void testFunctionWithNamedConstant(){
        Ogma ogma = new Ogma();
        ogma.addStringLiteral("test", "hello, world!");

        InternalRegexFunction function = new InternalRegexFunction(new OgmaParser(ogma, "function test() = test"));
        Assertions.assertEquals("hello, world!", function.getStringForm(new OgmaParser("test()")));

    }

    @Test
    public void testFunctionWith2UnorderedArguments(){
        InternalRegexFunction function = new InternalRegexFunction(new OgmaParser("function test(2) = $2 $1"));
        OgmaParser call = new OgmaParser("test(\" world!\", \"hello,\")");
        Assertions.assertEquals("hello, world!", function.getStringForm(call));
    }

    @Test
    public void testFunctionWithNestedFunctionCall(){
        Ogma ogma = new Ogma();
        ogma.addNamedComponent(new InternalRegexFunction(new OgmaParser(ogma, "function test(1) = \"hello\" $1")));

        InternalRegexFunction func = new InternalRegexFunction(new OgmaParser(ogma,"function test2(1) = test($1) \"world\""));
        Assertions.assertEquals("hello, world", func.getStringForm(new OgmaParser("test2(\", \")")));
    }

    @Test
    public void testDoubleNestedFunctionCall(){
        Ogma ogma = new Ogma();
        ogma.addNamedComponent(new InternalRegexFunction(new OgmaParser("function s(2) = $2 $1 $2")));
        InternalRegexFunction func = new InternalRegexFunction(new OgmaParser(ogma,"function p(2) = s(s($1, \"_\"), \"-\") \" & \" $2"));

        OgmaParser call = new OgmaParser(ogma, "p(\"hello\", s(\"world\", \"'\"))");
        Assertions.assertEquals("-_hello_- & 'world'", func.getStringForm(call));
    }

    @Test
    public void testWrongNumberOfArguments(){
        InternalRegexFunction func = new InternalRegexFunction(new OgmaParser("function test(0) = \"hello\""));
        Assertions.assertThrows(
                SyntaxErrorException.class,
                () -> func.getStringForm(new OgmaParser("test(\"world\")"))
        );
    }

    @Test
    public void testUnclosedNestedFunctionCall(){
        Ogma ogma = new Ogma();
        ogma.addNamedComponent(new InternalRegexFunction(new OgmaParser("function s(1) = $1")));
        Assertions.assertThrows(SyntaxErrorException.class,
                () -> new InternalRegexFunction(new OgmaParser("function test(1) = s(\"hello\"")));
    }

    @Test
    public void testMultipleConstants(){
        Ogma ogma = new Ogma();
        ogma.addStringLiteral("test", "hello, ");
        ogma.addStringLiteral("test2", "world!");
        InternalRegexFunction func = new InternalRegexFunction(new OgmaParser(ogma, "function f() = test test2"));
        Assertions.assertEquals(
                "hello, world!",
                    func.getStringForm(new OgmaParser(ogma, "f()"))
        );
    }
}
