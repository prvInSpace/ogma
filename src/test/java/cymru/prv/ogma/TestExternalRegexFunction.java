package cymru.prv.ogma;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TestExternalRegexFunction {

    @Test
    public void testFunctionWithNoParameters(){
        ExternalRegexFunction function = new ExternalRegexFunction("test", (s) -> "test");
        Assertions.assertEquals("test", function.getStringForm(new OgmaParser("test()")));
    }

    @Test
    public void testFunctionWith1Parameter(){
        ExternalRegexFunction function = new ExternalRegexFunction("test", (s) -> s[0]);
        OgmaParser parser = new OgmaParser("test(\"test\")");
        Assertions.assertEquals("test", function.getStringForm(parser));
    }

    @Test
    public void testFunctionWith2Parameters(){
        ExternalRegexFunction function = new ExternalRegexFunction("test", (s) -> s[0] + s[1]);
        OgmaParser parser = new OgmaParser("test(\"test\",\"hello\")");
        Assertions.assertEquals("testhello", function.getStringForm(parser));
    }

    @Test
    public void testGetName(){
        ExternalRegexFunction function = new ExternalRegexFunction("test", s -> null);
        Assertions.assertEquals("test", function.getName());
    }

    @Test
    public void testFunctionWithParameters(){
        ExternalRegexFunction function = new ExternalRegexFunction("test", s -> s[0]);
        Assertions.assertEquals("hello", function.getStringForm(new String[]{"hello"}));
    }

}
