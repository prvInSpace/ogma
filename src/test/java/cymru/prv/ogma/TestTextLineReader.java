package cymru.prv.ogma;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;
import java.util.List;

public class TestTextLineReader {

    private BufferedReader wrap(List<String> lines){
        return new BufferedReader(
            new StringReader(String.join("\n", lines))
        );
    }

    @Test
    public void testOgmaFileLineReader() throws IOException {
        List<String> lines = List.of(
                "Hello, World!",
                "Testing 123"
        );
        TextLineReader reader = new TextLineReader(wrap(lines));

        Assertions.assertArrayEquals(
                lines.get(0).split("\\s"),
                reader.getNext());
        Assertions.assertArrayEquals(
                lines.get(1).split("\\s"),
                reader.getNext());
        Assertions.assertNull(reader.getNext());
    }

}
