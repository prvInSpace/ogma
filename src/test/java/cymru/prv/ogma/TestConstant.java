package cymru.prv.ogma;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TestConstant {

    @Test
    public void testCreateNamedConstant(){
        OgmaParser declaration = new OgmaParser("const myConst = \"hello world\"");
        Constant constant = new Constant(declaration);
        OgmaParser reference = new OgmaParser("myConst");
        Assertions.assertEquals("hello world", constant.getStringForm(reference));
    }

    @Test
    public void testCreateConstantWithOtherNamedReference(){
        // Set up ogma with named constant
        Ogma ogma = new Ogma();
        ogma.addStringLiteral("test", " world!");

        Constant myConst = new Constant(new OgmaParser(ogma,"const myConst = \"hello,\" test"));
        Assertions.assertEquals(
                "hello, world!",
                myConst.getStringForm(new OgmaParser("myConst"))
        );
    }

    @Test
    public void testCreateConstantWithFunctionCall(){
        Ogma ogma = new Ogma();
        ogma.addNamedComponent(new ExternalRegexFunction("test", s -> "hello"));

        Constant myConst = new Constant(new OgmaParser(ogma,"const myConst = test() \", world!\""));
        Assertions.assertEquals(
                "hello, world!",
                myConst.getStringForm(new OgmaParser("myConst"))
        );
    }

}
