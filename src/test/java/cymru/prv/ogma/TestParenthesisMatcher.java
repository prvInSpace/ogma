package cymru.prv.ogma;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TestParenthesisMatcher {

    @Test
    public void testSingleCondition(){
        Matcher par = new ParenthesisMatcher(new OgmaParser("(\"hello\")"), "(", ")");
        Assertions.assertTrue(par.isMatch("hello"));
        Assertions.assertFalse(par.isMatch("world"));
    }

    @Test
    public void testOrCondition(){
        Matcher or = new ParenthesisMatcher(
                new OgmaParser("(\"hello\"|\"world\")"), "(", ")");
        Assertions.assertTrue(or.isMatch("hello"));
        Assertions.assertTrue(or.isMatch("world"));
        Assertions.assertFalse(or.isMatch("test"));
    }

    @Test
    public void testMultipleOrConditions(){
        Matcher matcher = new ParenthesisMatcher(
                new OgmaParser("(\"hello\"|\"world\"|\"test\")"), "(", ")");
        Assertions.assertTrue(matcher.isMatch("hello"));
        Assertions.assertTrue(matcher.isMatch("world"));
        Assertions.assertTrue(matcher.isMatch("test"));
        Assertions.assertFalse(matcher.isMatch("word"));
    }

    @Test
    public void testAndCondition(){
        Matcher matcher = new ParenthesisMatcher(
                new OgmaParser("(\"hello\"&\"world\")"),
                "(", ")"
        );
        Assertions.assertFalse(matcher.isMatch("hello"));
        Assertions.assertFalse(matcher.isMatch("world"));
        Assertions.assertTrue(matcher.isMatch("hello, world!"));
    }

    @Test
    public void testUnexpectedToken(){
        Assertions.assertThrows(
                SyntaxErrorException.class,
                () -> new ParenthesisMatcher(
                        new OgmaParser("(\"hello\"=\"world\")"), "(", ")")
        );
    }

    @Test
    public void testNamedConstant(){
        Ogma ogma = new Ogma();
        ogma.addStringLiteral("test", "hello");
        Matcher matcher = new ParenthesisMatcher(new OgmaParser(ogma, "(test)"), "(", ")");
        Assertions.assertTrue(matcher.isMatch("hello"));
        Assertions.assertFalse(matcher.isMatch("world"));
    }

    @Test
    public void testFunctionCall(){
        Ogma ogma = new Ogma();
        ogma.addNamedComponent(new ExternalRegexFunction("test", s -> s[0]));
        Matcher matcher = new ParenthesisMatcher(new OgmaParser(ogma, "(test(\"hello\"))"), "(", ")");
        Assertions.assertTrue(matcher.isMatch("hello"));
        Assertions.assertFalse(matcher.isMatch("world"));
    }

    @Test
    public void testNestedParenthesis(){
        OgmaParser parser = new OgmaParser("(\"hello\"&(\"world\"|\"ogma\"))");
        Matcher matcher = new ParenthesisMatcher(parser, "(", ")");
        Assertions.assertTrue(matcher.isMatch("hello, world!"));
        Assertions.assertTrue(matcher.isMatch("hello, ogma!"));
        Assertions.assertFalse(matcher.isMatch("hello, test!"));
        Assertions.assertFalse(matcher.isMatch("world"));
        Assertions.assertFalse(matcher.isMatch("hello"));
    }


    @Test
    public void testNotOperator(){
        OgmaParser parser = new OgmaParser("(!\"hello\"&!\"world\")");
        Matcher matcher = new ParenthesisMatcher(parser, "(", ")");
        Assertions.assertTrue(matcher.isMatch("test"));
        Assertions.assertFalse(matcher.isMatch("hello test"));
        Assertions.assertFalse(matcher.isMatch("test world"));
        Assertions.assertFalse(matcher.isMatch("hello world"));
    }
}
