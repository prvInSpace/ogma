package cymru.prv.ogma;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TestNamedMatcher {

    @Test
    public void testParseSimplePattern(){
        NamedMatcher matcher = new NamedMatcher(new OgmaParser("matcher test = \"hello\""));
        Assertions.assertTrue(matcher.getMatcher(new OgmaParser("test")).apply("hello"));
        Assertions.assertEquals("test", matcher.getName());
    }

    @Test
    public void testNestedPattern(){
        Ogma ogma = new Ogma();
        ogma.addNamedComponent(new NamedMatcher(new OgmaParser("matcher test = \"hello\"")));
        NamedMatcher matcher = new NamedMatcher(new OgmaParser(ogma ,"matcher test2 = test"));
        Assertions.assertTrue(matcher.getMatcher(new OgmaParser("test2")).apply("hello"));
    }

    @Test
    public void testEmptyPattern(){
        Assertions.assertThrows(
                SyntaxErrorException.class,
                () -> new NamedMatcher(new OgmaParser("matcher test = "))
        );
    }


}
