package cymru.prv.ogma;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;

public class TestExternalMatcherFunction {

    @Test
    public void testName(){
        ExternalMatcherFunction func = new ExternalMatcherFunction("test", null);
        Assertions.assertEquals("test", func.getName());
    }

    @Test
    public void testUseInResolvableShouldThrow(){
        Ogma ogma = new Ogma();
        ogma.addNamedComponent(new ExternalMatcherFunction("test", (s, t) -> true));
        Assertions.assertThrows(SyntaxErrorException.class, () ->
                new Constant(new OgmaParser(ogma, "const hello = test(\"hello\")"))
        );
    }

    @Test
    public void testShouldReturnTrueIfMatch(){
        ExternalMatcherFunction f = new ExternalMatcherFunction("test", (s, t) ->
                s.matches("hello")
        );
        Assertions.assertTrue(
                f.getMatcher(new OgmaParser("test()"))
                        .apply("hello")
        );
    }

    @Test
    public void testShouldReturnFalseIfMismatch(){
        ExternalMatcherFunction f = new ExternalMatcherFunction("test", (s, t) ->
                s.matches("hello")
        );
        Assertions.assertFalse(
                f.getMatcher(new OgmaParser("test()"))
                        .apply("world")
        );
    }

    @Test
    public void testShouldReturnTrueIfMatchFirstArgument(){
        ExternalMatcherFunction f = new ExternalMatcherFunction("test", (s, t) ->
                s.equals(t[0])
        );
        Assertions.assertTrue(
                f.getMatcher(new OgmaParser("test(\"hello\")"))
                    .apply("hello")
        );
    }

    @Test
    public void testWithFunctionCalls(){
        Ogma ogma = new Ogma();
        ogma.addNamedComponent(new ExternalRegexFunction("wrap", (s) ->
            "^" + s[0] + " world$"
        ));
        ExternalMatcherFunction f = new ExternalMatcherFunction("test", (s, t) ->
                s.matches(t[0])
        );
        Assertions.assertTrue(
                f.getMatcher(new OgmaParser(ogma, "test(wrap(\"hello\"))"))
                    .apply("hello world")
        );
    }

    @Test
    public void testInSearchPattern(){
        Ogma ogma = new Ogma();
        ogma.addNamedComponent(new ExternalMatcherFunction("test", (s, t) ->
            s.matches(t[0])
        ));

        SearchPattern pattern = new SearchPattern(
                new OgmaParser(ogma,"(test(\"hello\"))"
        ));
        List<Match> matches = pattern.getMatches(List.of("hello", "world"));
        Assertions.assertEquals(1, matches.size());
        Assertions.assertEquals("hello", matches.get(0).getWords().get(0));
    }

}
