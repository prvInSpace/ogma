package cymru.prv.ogma;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;

public class TestSearchPattern {

    @Test
    public void testSearchPatternWith1Target(){
        SearchPattern pattern = new SearchPattern(new OgmaParser("(\"hello\")"));
        List<Match> match = pattern.getMatches(List.of("hello", "world"));
        Assertions.assertEquals(1, match.size());
        Assertions.assertEquals(List.of("hello"), match.get(0).getWords());
        Assertions.assertEquals("hello", match.get(0).getTarget());
    }

    @Test
    public void testOptionalToken(){
        SearchPattern pattern = new SearchPattern(new OgmaParser("{\"hello\"}[\",\"](\"world\")"));
        List<Match> match = pattern.getMatches(List.of("hello", ",", "world"));

        Assertions.assertEquals(1, match.size());
        Assertions.assertEquals(List.of("hello", ",", "world"), match.get(0).getWords());
        Assertions.assertEquals("world", match.get(0).getTarget());
    }

    @Test
    public void testOptionalTokenWithoutToken(){
        SearchPattern pattern = new SearchPattern(new OgmaParser("{\"hello\"}[\",\"](\"world\")"));
        List<Match> match = pattern.getMatches(List.of("hello", "world"));

        Assertions.assertEquals(1, match.size());
        Assertions.assertEquals(1, match.get(0).getTargetIndex());
        Assertions.assertEquals(List.of("hello", "world"), match.get(0).getWords());
    }

    @Test
    public void testOptionalTokenWithMismatch(){
        SearchPattern pattern = new SearchPattern(new OgmaParser("{\"hello\"}[\",\"](\"world\")"));
        List<Match> match = pattern.getMatches(List.of("hello", "test", "world"));

        Assertions.assertEquals(0, match.size());
    }

    @Test
    public void testPatternInMiddleOfString(){
        SearchPattern pattern = new SearchPattern(new OgmaParser("(\"beautiful\")"));
        List<Match> match = pattern.getMatches(List.of("hello", "beautiful", "world"));
        Assertions.assertEquals(1, match.size());
        Assertions.assertIterableEquals(List.of("beautiful"), match.get(0).getWords());
    }

    @Test
    public void testRequiredToken(){
        SearchPattern pattern = new SearchPattern(new OgmaParser("{\"hello\"}(\"world\")"));
        List<Match> match = pattern.getMatches(List.of("hello", "world"));
        Assertions.assertEquals(1, match.size());
        Assertions.assertEquals(List.of("hello", "world"), match.get(0).getWords());
    }

    @Test
    public void testUnexpectedTokenShouldThrowException(){
        Assertions.assertThrows(
                SyntaxErrorException.class,
                () -> new SearchPattern(new OgmaParser("(\"hello\")/{\"world\"}"))
        );
    }

    @Test
    public void testGetFullSequenceFromMatch(){
        SearchPattern pattern = new SearchPattern(new OgmaParser("(\"hello\")"));
        List<Match> match = pattern.getMatches(List.of("hello", "world"));
        Assertions.assertEquals(1, match.size());
        Assertions.assertEquals(List.of("hello"), match.get(0).getWords());
        Assertions.assertIterableEquals(List.of("hello", "world"), match.get(0).getFullSequence());
    }

    @Test
    public void testNamedPattern(){
        SearchPattern pattern = new SearchPattern(new OgmaParser("pattern test = (\"hello\")"));
        Assertions.assertEquals("test", pattern.getName());
        List<Match> matches = pattern.getMatches(List.of("hello"));
        Assertions.assertEquals(1, matches.size());
        Assertions.assertEquals("hello", matches.get(0).getTarget());
        Assertions.assertEquals("test", matches.get(0).getSearchPatternName());
    }

    @Test
    public void testPatternNameWithoutDefinedName(){
        SearchPattern pattern = new SearchPattern(new OgmaParser("\t(\"hello\")"));
        Assertions.assertEquals("(\"hello\")", pattern.getName());
    }

    @Test
    public void testNamedMatcher(){
        Ogma ogma = new Ogma();
        ogma.addNamedComponent(new NamedMatcher(new OgmaParser("matcher test = \"hello\"")));
        SearchPattern pattern = new SearchPattern(new OgmaParser(ogma, "(test)"));
        List<Match> matches = pattern.getMatches(List.of("hello"));
        Assertions.assertEquals(1, matches.size());
        Assertions.assertEquals("hello", matches.get(0).getTarget());
        Assertions.assertEquals("(test)", matches.get(0).getSearchPatternName());
    }

    @Test
    public void testNoTargetShouldThrow(){
        Assertions.assertThrows(SyntaxErrorException.class, () ->
                new SearchPattern(new OgmaParser("{\"hello\"}"))
        );
    }

    @Test
    public void testMultipleTargetsShouldThrow(){
        Assertions.assertThrows(SyntaxErrorException.class, () ->
                new SearchPattern(new OgmaParser("(\"hello\")(\"world\")"))
        );
    }
}
